/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zénon
 */
public class TutorJPATest {
    
    public TutorJPATest() {
    }

    /**
     * Test of equals method, of class TutorJPA.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        TutorJPA instance = new TutorJPA(1);
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        
        expResult = true;
        result=instance.equals(instance);
        assertEquals(expResult,result);
        
        TutorJPA instance2 = new TutorJPA(5);
        expResult=false;
        result = instance.equals(instance2);
        assertEquals(expResult,result);
        
        instance2.setIdTutor(1);
        expResult=true;
        result=instance.equals(instance2);
        assertEquals(expResult,result);
        
    }

    /**
     * Test of toString method, of class TutorJPA.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TutorJPA instance = new TutorJPA();
        String expResult = "JPA.Tutor[ idTutor=null ]";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new TutorJPA(1);
        expResult = "JPA.Tutor[ idTutor=1 ]";
        result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new TutorJPA(5);
        expResult = "JPA.Tutor[ idTutor=5 ]";
        result = instance.toString();
        assertEquals(expResult, result);

    }
    
}
