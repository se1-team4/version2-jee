/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zénon
 */
public class InternJPATest {
    
    public InternJPATest() {
    }

    /**
     * Test of equals method, of class InternJPA.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        InternJPA instance = new InternJPA(1);
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        
        expResult = true;
        result=instance.equals(instance);
        assertEquals(expResult,result);
        
        InternJPA instance2 = new InternJPA(5);
        expResult=false;
        result = instance.equals(instance2);
        assertEquals(expResult,result);
        
        instance2.setIdIntern(1);
        expResult=true;
        result=instance.equals(instance2);
        assertEquals(expResult,result);
        
    }

    /**
     * Test of toString method, of class InternJPA.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        InternJPA instance = new InternJPA();
        String expResult = "JPA.Intern[ idIntern=null ]";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new InternJPA(1);
        expResult = "JPA.Intern[ idIntern=1 ]";
        result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new InternJPA(5);
        expResult = "JPA.Intern[ idIntern=5 ]";
        result = instance.toString();
        assertEquals(expResult, result);

    }
    
}
