/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zénon
 */
public class CompanyJPATest {
    
    public CompanyJPATest() {
    }

    /**
     * Test of equals method, of class CompanyJPA.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        CompanyJPA instance = new CompanyJPA(1);
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        
        expResult = true;
        result=instance.equals(instance);
        assertEquals(expResult,result);
        
        CompanyJPA instance2 = new CompanyJPA(5);
        expResult=false;
        result = instance.equals(instance2);
        assertEquals(expResult,result);
        
        instance2.setIdCompany(1);
        expResult=true;
        result=instance.equals(instance2);
        assertEquals(expResult,result);
        
    }

    /**
     * Test of toString method, of class CompanyJPA.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        CompanyJPA instance = new CompanyJPA();
        String expResult = "JPA.Company[ idCompany=null ]";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new CompanyJPA(1);
        expResult = "JPA.Company[ idCompany=1 ]";
        result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new CompanyJPA(5);
        expResult = "JPA.Company[ idCompany=5 ]";
        result = instance.toString();
        assertEquals(expResult, result);

    }
    
}
