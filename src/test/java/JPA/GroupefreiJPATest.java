/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zénon
 */
public class GroupefreiJPATest {
    
    public GroupefreiJPATest() {
    }

    /**
     * Test of equals method, of class GroupefreiJPA.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        GroupefreiJPA instance = new GroupefreiJPA();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        
        instance = new GroupefreiJPA();
        expResult = true;
        result = instance.equals(instance);
        assertEquals(expResult, result);
        
        instance = new GroupefreiJPA(1);
        GroupefreiJPA instance2=new GroupefreiJPA(2);
        expResult = false;
        result = instance.equals(instance2);
        assertEquals(expResult, result);
        
        instance = new GroupefreiJPA(1);
        instance2=new GroupefreiJPA(1);
        expResult = true;
        result = instance.equals(instance2);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class GroupefreiJPA.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        GroupefreiJPA instance = new GroupefreiJPA();
        String expResult = "JPA.Groupefrei[ idGroup=null ]";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new GroupefreiJPA(1);
        expResult = "JPA.Groupefrei[ idGroup=1 ]";
        result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new GroupefreiJPA(6);
        expResult = "JPA.Groupefrei[ idGroup=6 ]";
        result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
