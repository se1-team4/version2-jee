/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zénon
 */
public class InternshipJPATest {
    
    public InternshipJPATest() {
    }

    /**
     * Test of equals method, of class InternshipJPA.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        InternshipJPA instance = new InternshipJPA(1);
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        
        expResult = true;
        result=instance.equals(instance);
        assertEquals(expResult,result);
        
        InternshipJPA instance2 = new InternshipJPA(5);
        expResult=false;
        result = instance.equals(instance2);
        assertEquals(expResult,result);
        
        instance2.setIdInternship(1);
        expResult=true;
        result=instance.equals(instance2);
        assertEquals(expResult,result);
        
    }

    /**
     * Test of toString method, of class InternshipJPA.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        InternshipJPA instance = new InternshipJPA();
        String expResult = "JPA.Internship[ idInternship=null ]";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new InternshipJPA(1);
        expResult = "JPA.Internship[ idInternship=1 ]";
        result = instance.toString();
        assertEquals(expResult, result);
        
        instance = new InternshipJPA(5);
        expResult = "JPA.Internship[ idInternship=5 ]";
        result = instance.toString();
        assertEquals(expResult, result);

    }
    
}
