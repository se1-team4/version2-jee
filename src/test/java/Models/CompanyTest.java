/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Zénon
 */
public class CompanyTest {
    
    private Company instance;
    public CompanyTest() {
    }

    @Before
    public void setUp() {
        instance=new Company(1,"TestCompany","TestCompany_Adress");
    }
    
    /**
     * Test of IdProperty method, of class Company.
     */
    @Test
    public void testIdProperty() {
        System.out.println("IdProperty");
        int id = 1;
        int Result=instance.getId();
        assertEquals(Result, id);
        
        instance.setId(2);
        id=2;
        Result=instance.getId();
        assertEquals(Result, id);
    }


    /**
     * Test of NameProperty method, of class Company.
     */
    @Test
    public void testNameProperty() {
        System.out.println("NameProperty");
        String expResult = "TestCompany";
        String Result=instance.getName();
        assertEquals(expResult,Result);
        
        instance.setName("Company");
        expResult="Company";
        Result=instance.getName();
        assertEquals(expResult,Result);
    }

    /**
     * Test of AddressProperty method, of class Company.
     */
    @Test
    public void testAddressProperty() {
        System.out.println("AdressProperty");
        String expResult = "TestCompany_Adress";
        String Result=instance.getAddress();
        assertEquals(expResult,Result);
        
        instance.setAddress("Adress");
        expResult="Adress";
        Result=instance.getAddress();
        assertEquals(expResult,Result);
    }

    /**
     * Test of SupervisorProperty method, of class Company.
     */
    @Test
    public void testSupervisorProperty() {
        System.out.println("SupervisorProperty");
        String expResult = null;
        String Result=instance.getSupervisor();
        assertEquals(expResult,Result);
        
        instance.setSupervisor("TestSupervisor");
        expResult="TestSupervisor";
        Result=instance.getSupervisor();
        assertEquals(expResult,Result);
    }

    /**
     * Test of toString method, of class Company.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Company{" + "id=" + instance.getId() + ", name=" + instance.getName() + ", address=" + instance.getAddress() + ", supervisor=" + instance.getSupervisor() + '}';
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
