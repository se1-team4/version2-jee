/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zénon
 */
public class GroupTest {
    
    private Group instance;
    
    public GroupTest() {
    }
    
    @Before
    public void setUp() {
        instance=new Group(1,"testGroup");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of IdProperty method, of class Group.
     */
    @Test
    public void testIdProperty() {
        System.out.println("IdProperty");
        int id = 1;
        int Result=instance.getId();
        assertEquals(Result, id);
        
        instance.setId(2);
        id=2;
        Result=instance.getId();
        assertEquals(Result, id);
    }


    /**
     * Test of NameProperty method, of class Group.
     */
    @Test
    public void testNameProperty() {
        System.out.println("NameProperty");
        String expResult = "testGroup";
        String Result=instance.getName();
        assertEquals(expResult,Result);
        
        instance.setName("Group");
        expResult="Group";
        Result=instance.getName();
        assertEquals(expResult,Result);
    }


    /**
     * Test of toString method, of class Group.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Group{" + "id=" + instance.getId() + ", name=" + instance.getName() + '}';
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
