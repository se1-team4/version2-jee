/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zénon
 */
public class InternTest {
    
    private Intern instance;
    
    public InternTest() {
    }
    
    @Before
    public void setUp() {
        instance=new Intern(1,"TestLinkedIn","TestLastName","TestFirstName");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of IdProperty method, of class Intern.
     */
    @Test
    public void testIdProperty() {
        System.out.println("IdProperty");
        int id = 1;
        int Result=instance.getId();
        assertEquals(Result, id);
        
        instance.setId(2);
        id=2;
        Result=instance.getId();
        assertEquals(Result, id);
    }


    /**
     * Test of GroupProperty method, of class Intern.
     */
    @Test
    public void testGroupProperty() {
        System.out.println("GroupProperty");
        Group expResult = null;
        Group Result=instance.getGroup();
        assertEquals(expResult,Result);
        
        
        Group newGroupe=new Group(1,"TestGroup");
        instance.setGroup(newGroupe);
        expResult=newGroupe;
        Result=instance.getGroup();
        assertEquals(expResult,Result);
    }


    /**
     * Test of LinkedinProperty method, of class Intern.
     */
    @Test
    public void testLinkedinProperty() {
        System.out.println("LinkedInProperty");
        String expResult = "TestLinkedIn";
        String Result=instance.getLinkedin();
        assertEquals(expResult,Result);
        
        instance.setLinkedin("LinkedIn");
        expResult="LinkedIn";
        Result=instance.getLinkedin();
        assertEquals(expResult,Result);
    }

    /**
     * Test of ListInternshipProperty method, of class Intern.
     */
    @Test
    public void testListInternshipProperty() {
        System.out.println("ListInternshipProperty");
        ArrayList<Internship> expResult = null;
        ArrayList<Internship> Result = instance.getListInternship();
        assertEquals(expResult, Result);
        
        ArrayList<Internship> newListInternship=new ArrayList<>();
        newListInternship.add(new Internship(1, "TestDescription"));
        instance.setListInternship(newListInternship);
        expResult=newListInternship;
        Result=instance.getListInternship();
        assertEquals(expResult, Result);
        
    }

    /**
     * Test of toString method, of class Intern.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Intern{" + "id=" + instance.getId() + ", group=" + instance.getGroup() + ", linkedin=" + instance.getLinkedin() + ", internships : " + instance.getListInternship().size()+ '}';
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
