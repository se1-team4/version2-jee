/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.sql.Connection;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;
import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;
/**
 *
 * @author Zénon
 */

//This test doesn't work, for reasons explained below
//Tests were left as automatically generated, except for the fail() functions.
public class AuthTest {
    //@Mock
    private Auth instance; 
    private Auth instanceSpy;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    
    public AuthTest() {
        
    }
    
    @Before
    public void setUp() throws ServletException {
        //This initialization is vital to have any tests happen upon the servlet,
        //but no amount of tinkering around made it work.
        instance=new Auth();
        
        //As searched online, using mockito to handle the http servlet request and response, 
        //as well as either mocking or spying the servlet itself is the way to go
        instanceSpy=Mockito.spy(instance);
        MockitoAnnotations.initMocks(this);
        
    }


    /**
     * Test of checkCredential method, of class Auth.
     */
    @Test
    public void testCheckCredential() {
        System.out.println("checkCredential");
        HttpServletRequest request = null;
        boolean expResult = false;
        boolean result = instance.checkCredential(request);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isThereAnyArgs method, of class Auth.
     */
    @Test
    public void testIsThereAnyArgs() {
        System.out.println("isThereAnyArgs");
        
        when(request.getParameter("loginForm")).thenReturn("login");
        when(request.getParameter("passForm")).thenReturn("login");
        boolean expResult = true;
        boolean result = instance.isThereAnyArgs(request);
        assertEquals(expResult, result);
        
        when(request.getParameter("loginForm")).thenReturn(null);
        when(request.getParameter("passForm")).thenReturn("login");
        expResult = true;
        result = instance.isThereAnyArgs(request);
        assertEquals(expResult, result);
    }

    /**
     * Test of isLoginValid method, of class Auth.
     */
    @Test
    public void testIsLoginValid() throws Exception {
        System.out.println("isLoginValid");
        Connection conn = null;
        String login = "";
        boolean expResult = false;
        boolean result = instance.isLoginValid(conn, login);
        assertEquals(expResult, result);
    }

    /**
     * Test of isPasswordValid method, of class Auth.
     */
    @Test
    public void testIsPasswordValid() throws Exception {
        System.out.println("isPasswordValid");
        Connection conn = null;
        String password = "";
        String login = "";
        boolean expResult = false;
        boolean result = instance.isPasswordValid(conn, password, login);
        assertEquals(expResult, result);
    }
    
}
