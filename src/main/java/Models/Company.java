package Models;

import JPA.CompanyJPA;
import static utils.Constants.*;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alex
 */
public class Company {

    private int id;
    private String name;
    private String address;
    private String supervisor;

    public Company(int id, String name, String address, String supervisor) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.supervisor = supervisor.toUpperCase();
    }
    
    public Company(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.supervisor = DEFAULT_VALUE.toUpperCase();
    }
    
    public Company(){
        this.id = 0;
        this.name = DEFAULT_VALUE;
        this.address = DEFAULT_VALUE;
        this.supervisor = DEFAULT_VALUE.toUpperCase();
    }
    
    public Company(CompanyJPA companyjpa){
        this.id = companyjpa.getIdCompany();
        this.name = companyjpa.getNameCompany();
        this.address = companyjpa.getAddressCompany();
        this.supervisor = companyjpa.getSupervisorNameCompany();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor.toUpperCase();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Company other = (Company) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.supervisor, other.supervisor)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.id;
        hash = 71 * hash + Objects.hashCode(this.name);
        hash = 71 * hash + Objects.hashCode(this.address);
        hash = 71 * hash + Objects.hashCode(this.supervisor);
        return hash;
    }
    
    @Override
    public String toString() {
        return String.format("(%s)-[Id : %d, Name : %s, Address : %s, Supervisor : %s]", this.getClass().getSimpleName().toUpperCase(),id,name,address,supervisor);
    }

}
