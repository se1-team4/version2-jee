/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Alex
 */
public abstract class Person {
    
    private String lastname;
    private String firstname;

    public Person(String lastname, String firstname) {
        this.lastname = lastname.toUpperCase();
        this.firstname = firstname.substring(0, 1).toUpperCase() + firstname.substring(1).toLowerCase();
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname.toUpperCase();
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname.substring(0, 1).toUpperCase() + firstname.substring(1).toLowerCase();
    }

    @Override
    public String toString() {
        return String.format("(%s)", this.getClass().getSimpleName().toUpperCase());
    }

    
   
}
