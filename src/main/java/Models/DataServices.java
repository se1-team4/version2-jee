package Models;

import Controller.Auth;
import JPA.Assignedinternship;
import JPA.CompanyJPA;
import JPA.InternJPA;
import JPA.InternshipJPA;
import JPA.SessionBeans.AbstractFacade;
import JPA.SessionBeans.CompanyFacade;
import JPA.SessionBeans.InternshipFacade;
import JPA.SessionBeans.TutorFacade;
import JPA.TutorJPA;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import static utils.Constants.*;

/**
 *
 * @author trist
 */
public class DataServices {
	
	
	
	
	private TutorFacade tutorFacade;
        private InternshipFacade internshipFacade;
        private CompanyFacade companyFacade;
        private InternshipJPA internshipJPA;
        private CompanyJPA companyJPA;
        private Tutor tutor;
        private TutorJPA tutorJpa;
	private Collection<InternJPA> listOfInternsJPA;
        private ArrayList<Intern> listOfInterns;
        private Intern currIntern;
        private Collection<Assignedinternship> lAssigned;
        private ArrayList<Internship> listInternship;
        private Internship currInternship;
        
	
	
	boolean passCheck;
    boolean logiCheck;
	boolean valid;


    public DataServices(InternshipFacade internshipFacade, CompanyFacade companyFacade, TutorFacade tutorFacade){
        this.companyFacade = companyFacade;
        this.internshipFacade = internshipFacade;
        this.tutorFacade = tutorFacade;
    }



    public void updateData(Internship internship, Company company, HttpSession session) {

        internshipJPA = internshipFacade.find(internship.getId());
        internshipJPA.setVisitSheetInternship(internship.isVisitSheet());
        internshipJPA.setEvalEntrSheetInternship(internship.isEvalEntSheet());
        internshipJPA.setWebSurveyInternship(internship.isWebSurvey());
        internshipJPA.setReportInternship(internship.isReport());
        internshipJPA.setDefenseInternship(internship.isDefense());
        internshipJPA.setSpecificationInternship(internship.isSpecification());
        internshipJPA.setVisitPlanifiedInternship(internship.isVisitPlanified());
        internshipJPA.setVisitDoneInternship(internship.isVisitDone());
        internshipJPA.setMissionDescriptionInternship(internship.getMissionDesc());
        internshipJPA.setCommentInternship(internship.getComment());
        internshipJPA.setComGradeInternship(internship.getComGrade());
        internshipJPA.setTechGradeInternship(internship.getTechGrade());
        
        
        companyJPA = companyFacade.find(company.getId());
        companyJPA.setNameCompany(company.getName());
        companyJPA.setAddressCompany(company.getAddress());
        companyJPA.setSupervisorNameCompany(company.getSupervisor());
        
        
        internshipFacade.edit(internshipJPA);
        companyFacade.edit(companyJPA);


    }

    public void loadData(HttpSession session) {
		
		
		tutor = (Tutor) session.getAttribute(SESSION_USER);
		
		tutorJpa = tutorFacade.findWithCredentials(tutor.getLogin(), tutor.getPassword());
		
		tutor.setId(tutorJpa.getIdTutor());
                tutor.setFirstname(tutorJpa.getFirstnameTutor());
                tutor.setLastname(tutorJpa.getLastnameTutor());
                tutor.setLogin(tutorJpa.getLoginTutor());
                tutor.setPassword(tutorJpa.getPasswordTutor());
                
                //set the list of intern is more tricky here
                listOfInternsJPA = tutorJpa.getInternCollection();
                listOfInterns = new ArrayList<>();
                
                for(InternJPA internJPA : listOfInternsJPA){
                    currIntern = new Intern();
                    currIntern.setFirstname(internJPA.getFirstnameIntern());
                    currIntern.setId(internJPA.getIdIntern());
                    currIntern.setLastname(internJPA.getLastnameIntern());
                    currIntern.setLinkedin(internJPA.getLinkedinIntern());
                    currIntern.setGroup(new Group(internJPA.getGroupIntern()));

                    
                    //need also to create a list for internship
                    
                    lAssigned = internJPA.getAssignedinternshipCollection();
                    listInternship = new ArrayList<>();
                    
                    for(Assignedinternship ai: lAssigned){
                        internshipJPA =  ai.getInternshipJPA();
                        currInternship = new Internship(internshipJPA);
                        currInternship.setCompany(new Company(ai.getCompanyJPA()));
                        listInternship.add(currInternship);
                    }
                    
                    currIntern.setListInternship(listInternship);
                    listOfInterns.add(currIntern);
                    
                }
                
                
                tutor.setListeIntern(listOfInterns);
		
		

		session.setAttribute(SESSION_USER, tutor);

     
    }

    public boolean checkCredential(HttpServletRequest request) {

        passCheck = false;
        logiCheck = false;
        
        /*LOADING DRIVER*/
        
        try {
            /*CHECK LOGIN*/
            logiCheck = isLoginValid(request.getParameter("loginForm"));
            if (logiCheck) {
                /*LOGIN IS CORRECT*/
                passCheck = isPasswordValid(request.getParameter("passForm"), request.getParameter("loginForm"));
                /*CHECK PASSWORD*/
                if (!passCheck) {
                    /*ADDING ERROR IN REQUEST*/
                    request.setAttribute("statusPassword", "Invalid credential try again");
                }
            } else {
                /*LOGIN IS INCORRECT*/
                /*ADDING ERROR IN REQUEST*/
                request.setAttribute("statusLogin", "Invalid Login try again");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        /*RETURNING THE STATE OF PASSWORD AND LOGIN*/
        return (passCheck && logiCheck);
    }

    private boolean isPasswordValid(String password, String login) throws SQLException {

        tutorJpa = tutorFacade.findWithCredentials(login, password);
        
        valid = (tutorJpa == null ? false : true);

        return valid;
    }

    private boolean isLoginValid(String login) throws SQLException {

		valid = tutorFacade.exist(login);
        
        return valid;
	}

}
