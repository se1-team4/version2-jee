/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import static utils.Constants.*;

/**
 *
 * @author Alex
 */
public class Tutor extends Person{
    
    private int id;
    private String login;
    private String password;/*OPTIONAL ?*/
    private ArrayList<Intern> interns = new ArrayList<Intern>();

    public Tutor(int id, String login, String password, String lastname, String firstname) {
        super(lastname, firstname);
        this.id = id;
        this.login = login;
        this.password = password;
    }
    
    public Tutor(){
        super(DEFAULT_VALUE, DEFAULT_VALUE);
        this.id = 0;
        this.login = DEFAULT_VALUE;
        this.password = DEFAULT_VALUE;
    }
    
    @Override
    public String getLastname(){
        return super.getLastname();
    }
    
    @Override
    public void setLastname(String Lastname){
        super.setLastname(Lastname);
    }
    
    @Override
    public String getFirstname(){
        return super.getFirstname();
    }
    
    @Override
    public void setFirstname(String Firstname){
        super.setFirstname(Firstname);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Intern> getInterns() {
        return interns;
    }

    public void setListeIntern(ArrayList<Intern> interns) {
        this.interns = interns;
    }
    
    public Intern getInternById(int id){
        Intern tempIntern = new Intern();
        for(Intern intern : interns){
            if(intern.getId() == id){
                tempIntern = intern;
            }
        }
        return tempIntern;
    }

    @Override
    public String toString() {
        return String.format("(%s)-[Id : %d, Lastname : %s, Firstname : %s, Login : %s, Interns nb : %d]", this.getClass().getSimpleName().toUpperCase(),id,super.getLastname(),super.getFirstname(),login,this.getInterns().size());
    }

    
}
