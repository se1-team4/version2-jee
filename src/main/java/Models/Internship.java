/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import JPA.InternshipJPA;
import java.util.Objects;
import static utils.Constants.*;

/**
 *
 * @author Alex
 */
public class Internship {
    private int id;
    private String dateStart;
    private String dateEnd;
    private Tutor tutor;
    private Company company;
    private Intern intern;
    private String missionDesc;
    private String comment;
    private float techGrade;
    private float comGrade;
    private boolean specification;
    private boolean visitSheet;
    private boolean evalEntSheet;
    private boolean webSurvey;
    private boolean report;
    private boolean defense;
    private boolean visitPlanified;
    private boolean visitDone;

    public Internship(int id, String dateStart, String dateEnd, Tutor tutor, Company company, Intern intern, String missionDesc,String comment, float techGrade, float comGrade, boolean specification, boolean visitSheet, boolean evalEntSheet, boolean webSurvey, boolean report, boolean defense, boolean visitPlanified, boolean visitDone) {
        this.id = id;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.tutor = tutor;
        this.company = company;
        this.intern = intern;
        this.missionDesc = missionDesc;
        this.comment = comment;
        this.techGrade = techGrade;
        this.comGrade = comGrade;
        this.specification = specification;
        this.visitSheet = visitSheet;
        this.evalEntSheet = evalEntSheet;
        this.webSurvey = webSurvey;
        this.report = report;
        this.defense = defense;
        this.visitPlanified = visitPlanified;
        this.visitDone = visitDone;
    }
    
    public Internship(int id, String dateStart, String dateEnd, String missionDesc,String comment, float techGrade, float comGrade, boolean specification, boolean visitSheet, boolean evalEntSheet, boolean webSurvey, boolean report, boolean defense, boolean visitPlanified, boolean visitDone) {
        this.id = id;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.missionDesc = missionDesc;
        this.comment = comment;
        this.techGrade = techGrade;
        this.comGrade = comGrade;
        this.specification = specification;
        this.visitSheet = visitSheet;
        this.evalEntSheet = evalEntSheet;
        this.webSurvey = webSurvey;
        this.report = report;
        this.defense = defense;
        this.visitPlanified = visitPlanified;
        this.visitDone = visitDone;
    }
    
    public Internship(InternshipJPA iJPA){
        this.id = iJPA.getIdInternship();
        this.dateStart = iJPA.getDateStartInternship().toString();
        this.dateEnd = iJPA.getDateEndInternship().toString();
        this.missionDesc = iJPA.getMissionDescriptionInternship();
        this.comment = iJPA.getCommentInternship();
        this.techGrade = iJPA.getTechGradeInternship();
        this.comGrade = iJPA.getComGradeInternship();
        this.specification = iJPA.getSpecificationInternship();
        this.visitSheet = iJPA.getVisitSheetInternship();
        this.evalEntSheet = iJPA.getEvalEntrSheetInternship();
        this.webSurvey = iJPA.getWebSurveyInternship();
        this.report = iJPA.getReportInternship();
        this.defense = iJPA.getDefenseInternship();
        this.visitPlanified = iJPA.getVisitPlanifiedInternship();
        this.visitDone = iJPA.getVisitDoneInternship();
        
    }
    
    public Internship(int id, String missionDesc) {
        this.id = id;
        this.dateStart = DEFAULT_VALUE;
        this.dateEnd = DEFAULT_VALUE;
        this.tutor = new Tutor();
        this.company = new Company();
        this.intern = new Intern();
        this.missionDesc = missionDesc;
        this.techGrade = 0f;
        this.comGrade = 0f;
        this.specification = false;
        this.visitSheet = false;
        this.evalEntSheet = false;
        this.webSurvey = false;
        this.report = false;
        this.defense = false;
        this.visitPlanified = false;
        this.visitDone = false;
        this.comment = DEFAULT_VALUE;
    }
    
    public Internship() {
        this.id = 0;
        this.dateStart = DEFAULT_VALUE;
        this.dateEnd = DEFAULT_VALUE;
        this.tutor = new Tutor();
        this.company = new Company();
        this.intern = new Intern();
        this.missionDesc = DEFAULT_VALUE;
        this.techGrade = 0f;
        this.comGrade = 0f;
        this.specification = false;
        this.visitSheet = false;
        this.evalEntSheet = false;
        this.webSurvey = false;
        this.report = false;
        this.defense = false;
        this.visitPlanified = false;
        this.visitDone = false;
        this.comment = DEFAULT_VALUE;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Intern getIntern() {
        return intern;
    }

    public void setIntern(Intern intern) {
        this.intern = intern;
    }

    public String getMissionDesc() {
        return missionDesc;
    }

    public void setMissionDesc(String missionDesc) {
        this.missionDesc = missionDesc;
    }

    public float getTechGrade() {
        return techGrade;
    }

    public void setTechGrade(float techGrade) {
        this.techGrade = techGrade;
    }

    public float getComGrade() {
        return comGrade;
    }

    public void setComGrade(float comGrade) {
        this.comGrade = comGrade;
    }

    public boolean isSpecification() {
        return specification;
    }

    public void setSpecification(boolean specification) {
        this.specification = specification;
    }

    public boolean isVisitSheet() {
        return visitSheet;
    }

    public void setVisitSheet(boolean visitSheet) {
        this.visitSheet = visitSheet;
    }

    public boolean isEvalEntSheet() {
        return evalEntSheet;
    }

    public void setEvalEntSheet(boolean evalEntSheet) {
        this.evalEntSheet = evalEntSheet;
    }

    public boolean isWebSurvey() {
        return webSurvey;
    }

    public void setWebSurvey(boolean webSurvey) {
        this.webSurvey = webSurvey;
    }

    public boolean isReport() {
        return report;
    }

    public void setReport(boolean report) {
        this.report = report;
    }

    public boolean isDefense() {
        return defense;
    }

    public void setDefense(boolean defense) {
        this.defense = defense;
    }

    public boolean isVisitPlanified() {
        return visitPlanified;
    }

    public void setVisitPlanified(boolean visitPlanified) {
        this.visitPlanified = visitPlanified;
    }

    public boolean isVisitDone() {
        return visitDone;
    }

    public void setVisitDone(boolean visitDone) {
        this.visitDone = visitDone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.company);
        hash = 97 * hash + Objects.hashCode(this.missionDesc);
        hash = 97 * hash + Objects.hashCode(this.comment);
        hash = 97 * hash + Float.floatToIntBits(this.techGrade);
        hash = 97 * hash + Float.floatToIntBits(this.comGrade);
        hash = 97 * hash + (this.specification ? 1 : 0);
        hash = 97 * hash + (this.visitSheet ? 1 : 0);
        hash = 97 * hash + (this.evalEntSheet ? 1 : 0);
        hash = 97 * hash + (this.webSurvey ? 1 : 0);
        hash = 97 * hash + (this.report ? 1 : 0);
        hash = 97 * hash + (this.defense ? 1 : 0);
        hash = 97 * hash + (this.visitPlanified ? 1 : 0);
        hash = 97 * hash + (this.visitDone ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Internship other = (Internship) obj;
        if (Float.floatToIntBits(this.techGrade) != Float.floatToIntBits(other.techGrade)) {
            return false;
        }
        if (Float.floatToIntBits(this.comGrade) != Float.floatToIntBits(other.comGrade)) {
            return false;
        }
        if (this.specification != other.specification) {
            return false;
        }
        if (this.visitSheet != other.visitSheet) {
            return false;
        }
        if (this.evalEntSheet != other.evalEntSheet) {
            return false;
        }
        if (this.webSurvey != other.webSurvey) {
            return false;
        }
        if (this.report != other.report) {
            return false;
        }
        if (this.defense != other.defense) {
            return false;
        }
        if (this.visitPlanified != other.visitPlanified) {
            return false;
        }
        if (this.visitDone != other.visitDone) {
            return false;
        }
        if (!Objects.equals(this.missionDesc, other.missionDesc)) {
            return false;
        }
        if (!Objects.equals(this.comment, other.comment)) {
            return false;
        }
        if (!Objects.equals(this.company, other.company)) {
            return false;
        }
        return true;
    }

    

    @Override
    public String toString() {
        return String.format("(%s)-[Id : %d, Start : %s, End : %s, Tutor : %s, Company : %s, Intern : %s, Description : %s]", this.getClass().getSimpleName().toUpperCase(),id,dateStart,dateEnd,tutor.getLastname()+","+tutor.getFirstname(),company.getName(),intern.getLastname()+","+intern.getFirstname(),missionDesc);
    }
    
    
}
