/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import static utils.Constants.*;

/**
 *
 * @author Alex
 */
public class Intern extends Person{
    
    private int id;
    private Group group;
    private String linkedin;
    private ArrayList<Internship> internships = new ArrayList<Internship>();

    public Intern(int id, Group group, String linkedin, String lastname, String firstname) {
        super(lastname, firstname);
        this.id = id;
        this.group = group;
        this.linkedin = linkedin;
    }
    
    public Intern(int id, String linkedin, String lastname, String firstname) {
        super(lastname, firstname);
        this.id = id;
        this.linkedin = linkedin;
        group = new Group();
    }
    
    public Intern(){
        super(DEFAULT_VALUE, DEFAULT_VALUE);
        id = 0;
        group = new Group();
        linkedin = DEFAULT_VALUE;
    }
    
    @Override
    public String getLastname(){
        return super.getLastname();
    }
    
    @Override
    public void setLastname(String Lastname){
        super.setLastname(Lastname);
    }
    
    @Override
    public String getFirstname(){
        return super.getFirstname();
    }
    
    @Override
    public void setFirstname(String Firstname){
        super.setFirstname(Firstname);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public ArrayList<Internship> getInternships() {
        return internships;
    }

    public void setListInternship(ArrayList<Internship> internships) {
        this.internships = internships;
    }
    
    public Internship getInternshipById(int id){
        Internship tempInternship = new Internship();
        for(Internship internship : internships){
            if(internship.getId() == id){
                tempInternship = internship;
            }
        }
        return tempInternship;
    }

    @Override
    public String toString() {
        return String.format("(%s)-[Id : %d, Lastname : %s, Firstname : %s, Group : %s, Linkedin : %s, Internship nb : %d]", this.getClass().getSimpleName().toUpperCase(),id,this.getLastname(),this.getFirstname(),group.getName(),linkedin,internships.size());
    }
    
    
            
}
