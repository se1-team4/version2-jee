/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import JPA.GroupefreiJPA;
import static utils.Constants.*;
/**
 *
 * @author Alex
 */
public class Group {
    private int id;
    private String name;
    
    public Group(){
        id = 0;
        name = DEFAULT_VALUE;
    }

    public Group(int id, String name) {
        this.id = id;
        this.name = name.toUpperCase();
    }
    
    public Group(GroupefreiJPA gJPA){
        this.id = gJPA.getIdGroup();
        this.name = gJPA.getNameGroup();
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    @Override
    public String toString() {
        return String.format("(%s)-[Id : %d, Name : %s]", this.getClass().getSimpleName().toUpperCase(),id,name);
    }
    
    
}
