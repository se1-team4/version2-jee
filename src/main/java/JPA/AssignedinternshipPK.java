/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Maxence Chambrin
 */
@Embeddable
public class AssignedinternshipPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_Internship")
    private int idInternship;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_Intern")
    private int idIntern;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_Company")
    private int idCompany;

    public AssignedinternshipPK() {
    }

    public AssignedinternshipPK(int idInternship, int idIntern, int idCompany) {
        this.idInternship = idInternship;
        this.idIntern = idIntern;
        this.idCompany = idCompany;
    }

    public int getIdInternship() {
        return idInternship;
    }

    public void setIdInternship(int idInternship) {
        this.idInternship = idInternship;
    }

    public int getIdIntern() {
        return idIntern;
    }

    public void setIdIntern(int idIntern) {
        this.idIntern = idIntern;
    }

    public int getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(int idCompany) {
        this.idCompany = idCompany;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idInternship;
        hash += (int) idIntern;
        hash += (int) idCompany;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssignedinternshipPK)) {
            return false;
        }
        AssignedinternshipPK other = (AssignedinternshipPK) object;
        if (this.idInternship != other.idInternship) {
            return false;
        }
        if (this.idIntern != other.idIntern) {
            return false;
        }
        if (this.idCompany != other.idCompany) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.AssignedinternshipPK[ idInternship=" + idInternship + ", idIntern=" + idIntern + ", idCompany=" + idCompany + " ]";
    }
    
}
