package JPA;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Maxence Chambrin
 */
@Entity
@Table(name = "tutor")
@NamedQueries({
    @NamedQuery(name = "Tutor.findAll", query = "SELECT t FROM TutorJPA t"),
    @NamedQuery(name = "Tutor.findByIdTutor", query = "SELECT t FROM TutorJPA t WHERE t.idTutor = :idTutor"),
    @NamedQuery(name = "Tutor.findByLoginTutor", query = "SELECT t FROM TutorJPA t WHERE t.loginTutor = :loginTutor"),
    @NamedQuery(name = "Tutor.findByPasswordTutor", query = "SELECT t FROM TutorJPA t WHERE t.passwordTutor = :passwordTutor"),
    @NamedQuery(name = "Tutor.findByFirstnameTutor", query = "SELECT t FROM TutorJPA t WHERE t.firstnameTutor = :firstnameTutor"),
    @NamedQuery(name = "Tutor.findByLastnameTutor", query = "SELECT t FROM TutorJPA t WHERE t.lastnameTutor = :lastnameTutor"),
    @NamedQuery(name = "Tutor.findWithCredentials", query = "SELECT t FROM TutorJPA t WHERE t.loginTutor = :loginTutor AND t.passwordTutor = :passwordTutor")
})
   
public class TutorJPA implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Tutor")
    private Integer idTutor;
    @Size(max = 320)
    @Column(name = "login_Tutor")
    private String loginTutor;
    @Size(max = 255)
    @Column(name = "password_Tutor")
    private String passwordTutor;
    @Size(max = 255)
    @Column(name = "firstname_Tutor")
    private String firstnameTutor;
    @Size(max = 255)
    @Column(name = "lastname_Tutor")
    private String lastnameTutor;
    @ManyToMany(mappedBy = "tutorCollection")
    private Collection<InternJPA> internCollection;

    public TutorJPA() {
    }

    public TutorJPA(Integer idTutor) {
        this.idTutor = idTutor;
    }

    public Integer getIdTutor() {
        return idTutor;
    }

    public void setIdTutor(Integer idTutor) {
        this.idTutor = idTutor;
    }

    public String getLoginTutor() {
        return loginTutor;
    }

    public void setLoginTutor(String loginTutor) {
        this.loginTutor = loginTutor;
    }

    public String getPasswordTutor() {
        return passwordTutor;
    }

    public void setPasswordTutor(String passwordTutor) {
        this.passwordTutor = passwordTutor;
    }

    public String getFirstnameTutor() {
        return firstnameTutor;
    }

    public void setFirstnameTutor(String firstnameTutor) {
        this.firstnameTutor = firstnameTutor;
    }

    public String getLastnameTutor() {
        return lastnameTutor;
    }

    public void setLastnameTutor(String lastnameTutor) {
        this.lastnameTutor = lastnameTutor;
    }

    public Collection<InternJPA> getInternCollection() {
        return internCollection;
    }

    public void setInternCollection(Collection<InternJPA> internCollection) {
        this.internCollection = internCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTutor != null ? idTutor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TutorJPA)) {
            return false;
        }
        TutorJPA other = (TutorJPA) object;
        if ((this.idTutor == null && other.idTutor != null) || (this.idTutor != null && !this.idTutor.equals(other.idTutor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.Tutor[ idTutor=" + idTutor + " ]";
    }
    
}
