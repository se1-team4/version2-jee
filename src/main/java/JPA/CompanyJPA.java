/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Maxence Chambrin
 */
@Entity
@Table(name = "company")
@NamedQueries({
    @NamedQuery(name = "Company.findAll", query = "SELECT c FROM CompanyJPA c"),
    @NamedQuery(name = "Company.findByIdCompany", query = "SELECT c FROM CompanyJPA c WHERE c.idCompany = :idCompany"),
    @NamedQuery(name = "Company.findByNameCompany", query = "SELECT c FROM CompanyJPA c WHERE c.nameCompany = :nameCompany"),
    @NamedQuery(name = "Company.findByAddressCompany", query = "SELECT c FROM CompanyJPA c WHERE c.addressCompany = :addressCompany"),
    @NamedQuery(name = "Company.findBySupervisorNameCompany", query = "SELECT c FROM CompanyJPA c WHERE c.supervisorNameCompany = :supervisorNameCompany")})
public class CompanyJPA implements Serializable {

    @Size(max = 255)
    @Column(name = "name_Company")
    private String nameCompany;
    @Size(max = 255)
    @Column(name = "address_Company")
    private String addressCompany;
    @Size(max = 255)
    @Column(name = "supervisorName_Company")
    private String supervisorNameCompany;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "companyJPA")
    private Collection<Assignedinternship> assignedinternshipCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Company")
    private Integer idCompany;

    public CompanyJPA() {
    }

    public CompanyJPA(Integer idCompany) {
        this.idCompany = idCompany;
    }

    public Integer getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(Integer idCompany) {
        this.idCompany = idCompany;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getAddressCompany() {
        return addressCompany;
    }

    public void setAddressCompany(String addressCompany) {
        this.addressCompany = addressCompany;
    }

    public String getSupervisorNameCompany() {
        return supervisorNameCompany;
    }

    public void setSupervisorNameCompany(String supervisorNameCompany) {
        this.supervisorNameCompany = supervisorNameCompany;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompany != null ? idCompany.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompanyJPA)) {
            return false;
        }
        CompanyJPA other = (CompanyJPA) object;
        if ((this.idCompany == null && other.idCompany != null) || (this.idCompany != null && !this.idCompany.equals(other.idCompany))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.Company[ idCompany=" + idCompany + " ]";
    }


    @XmlTransient
    public Collection<Assignedinternship> getAssignedinternshipCollection() {
        return assignedinternshipCollection;
    }

    public void setAssignedinternshipCollection(Collection<Assignedinternship> assignedinternshipCollection) {
        this.assignedinternshipCollection = assignedinternshipCollection;
    }
    
}
