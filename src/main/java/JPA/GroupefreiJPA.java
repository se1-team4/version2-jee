/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import Models.Group;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Maxence Chambrin
 */
@Entity
@Table(name = "groupefrei")
@NamedQueries({
    @NamedQuery(name = "Groupefrei.findAll", query = "SELECT g FROM GroupefreiJPA g"),
    @NamedQuery(name = "Groupefrei.findByIdGroup", query = "SELECT g FROM GroupefreiJPA g WHERE g.idGroup = :idGroup"),
    @NamedQuery(name = "Groupefrei.findByNameGroup", query = "SELECT g FROM GroupefreiJPA g WHERE g.nameGroup = :nameGroup")})
public class GroupefreiJPA implements Serializable {

    @Size(max = 25)
    @Column(name = "name_Group")
    private String nameGroup;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Group")
    private Integer idGroup;
    @OneToMany(mappedBy = "groupIntern")
    private Collection<InternJPA> internCollection;

    public GroupefreiJPA() {
    }

    public GroupefreiJPA(Integer idGroup) {
        this.idGroup = idGroup;
    }

    public Integer getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(Integer idGroup) {
        this.idGroup = idGroup;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    public Collection<InternJPA> getInternCollection() {
        return internCollection;
    }

    public void setInternCollection(Collection<InternJPA> internCollection) {
        this.internCollection = internCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGroup != null ? idGroup.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupefreiJPA)) {
            return false;
        }
        GroupefreiJPA other = (GroupefreiJPA) object;
        if ((this.idGroup == null && other.idGroup != null) || (this.idGroup != null && !this.idGroup.equals(other.idGroup))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.Groupefrei[ idGroup=" + idGroup + " ]";
    }
    
    public Group getBean(){
        return new Group(idGroup,nameGroup);
    }

}
