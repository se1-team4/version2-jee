/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Maxence Chambrin
 */
@Entity
@Table(name = "assignedinternship")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Assignedinternship.findAll", query = "SELECT a FROM Assignedinternship a"),
    @NamedQuery(name = "Assignedinternship.findByIdInternship", query = "SELECT a FROM Assignedinternship a WHERE a.assignedinternshipPK.idInternship = :idInternship"),
    @NamedQuery(name = "Assignedinternship.findByIdIntern", query = "SELECT a FROM Assignedinternship a WHERE a.assignedinternshipPK.idIntern = :idIntern"),
    @NamedQuery(name = "Assignedinternship.findByIdCompany", query = "SELECT a FROM Assignedinternship a WHERE a.assignedinternshipPK.idCompany = :idCompany")})
public class Assignedinternship implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AssignedinternshipPK assignedinternshipPK;
    @JoinColumn(name = "id_Company", referencedColumnName = "id_Company", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CompanyJPA companyJPA;
    @JoinColumn(name = "id_Intern", referencedColumnName = "id_Intern", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private InternJPA internJPA;
    @JoinColumn(name = "id_Internship", referencedColumnName = "id_Internship", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private InternshipJPA internshipJPA;

    public Assignedinternship() {
    }

    public Assignedinternship(AssignedinternshipPK assignedinternshipPK) {
        this.assignedinternshipPK = assignedinternshipPK;
    }

    public Assignedinternship(int idInternship, int idIntern, int idCompany) {
        this.assignedinternshipPK = new AssignedinternshipPK(idInternship, idIntern, idCompany);
    }

    public AssignedinternshipPK getAssignedinternshipPK() {
        return assignedinternshipPK;
    }

    public void setAssignedinternshipPK(AssignedinternshipPK assignedinternshipPK) {
        this.assignedinternshipPK = assignedinternshipPK;
    }

    public CompanyJPA getCompanyJPA() {
        return companyJPA;
    }

    public void setCompanyJPA(CompanyJPA companyJPA) {
        this.companyJPA = companyJPA;
    }

    public InternJPA getInternJPA() {
        return internJPA;
    }

    public void setInternJPA(InternJPA internJPA) {
        this.internJPA = internJPA;
    }

    public InternshipJPA getInternshipJPA() {
        return internshipJPA;
    }

    public void setInternshipJPA(InternshipJPA internshipJPA) {
        this.internshipJPA = internshipJPA;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (assignedinternshipPK != null ? assignedinternshipPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Assignedinternship)) {
            return false;
        }
        Assignedinternship other = (Assignedinternship) object;
        if ((this.assignedinternshipPK == null && other.assignedinternshipPK != null) || (this.assignedinternshipPK != null && !this.assignedinternshipPK.equals(other.assignedinternshipPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.Assignedinternship[ assignedinternshipPK=" + assignedinternshipPK + " ]";
    }
    
}
