/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import Models.Internship;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Maxence Chambrin
 */
@Entity
@Table(name = "internship")
@NamedQueries({
    @NamedQuery(name = "Internship.findAll", query = "SELECT i FROM InternshipJPA i"),
    @NamedQuery(name = "Internship.findByIdInternship", query = "SELECT i FROM InternshipJPA i WHERE i.idInternship = :idInternship"),
    @NamedQuery(name = "Internship.findByDateStartInternship", query = "SELECT i FROM InternshipJPA i WHERE i.dateStartInternship = :dateStartInternship"),
    @NamedQuery(name = "Internship.findByDateEndInternship", query = "SELECT i FROM InternshipJPA i WHERE i.dateEndInternship = :dateEndInternship"),
    @NamedQuery(name = "Internship.findByTechGradeInternship", query = "SELECT i FROM InternshipJPA i WHERE i.techGradeInternship = :techGradeInternship"),
    @NamedQuery(name = "Internship.findByComGradeInternship", query = "SELECT i FROM InternshipJPA i WHERE i.comGradeInternship = :comGradeInternship"),
    @NamedQuery(name = "Internship.findBySpecificationInternship", query = "SELECT i FROM InternshipJPA i WHERE i.specificationInternship = :specificationInternship"),
    @NamedQuery(name = "Internship.findByVisitSheetInternship", query = "SELECT i FROM InternshipJPA i WHERE i.visitSheetInternship = :visitSheetInternship"),
    @NamedQuery(name = "Internship.findByEvalEntrSheetInternship", query = "SELECT i FROM InternshipJPA i WHERE i.evalEntrSheetInternship = :evalEntrSheetInternship"),
    @NamedQuery(name = "Internship.findByWebSurveyInternship", query = "SELECT i FROM InternshipJPA i WHERE i.webSurveyInternship = :webSurveyInternship"),
    @NamedQuery(name = "Internship.findByReportInternship", query = "SELECT i FROM InternshipJPA i WHERE i.reportInternship = :reportInternship"),
    @NamedQuery(name = "Internship.findByDefenseInternship", query = "SELECT i FROM InternshipJPA i WHERE i.defenseInternship = :defenseInternship"),
    @NamedQuery(name = "Internship.findByVisitPlanifiedInternship", query = "SELECT i FROM InternshipJPA i WHERE i.visitPlanifiedInternship = :visitPlanifiedInternship"),
    @NamedQuery(name = "Internship.findByVisitDoneInternship", query = "SELECT i FROM InternshipJPA i WHERE i.visitDoneInternship = :visitDoneInternship")})
public class InternshipJPA implements Serializable {

    @Lob
    @Size(max = 65535)
    @Column(name = "missionDescription_Internship")
    private String missionDescriptionInternship;
    @Lob
    @Size(max = 65535)
    @Column(name = "comment_Internship")
    private String commentInternship;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "internshipJPA")
    private Collection<Assignedinternship> assignedinternshipCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Internship")
    private Integer idInternship;
    @Column(name = "dateStart_Internship")
    @Temporal(TemporalType.DATE)
    private Date dateStartInternship;
    @Column(name = "dateEnd_Internship")
    @Temporal(TemporalType.DATE)
    private Date dateEndInternship;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "techGrade_Internship")
    private Float techGradeInternship;
    @Column(name = "comGrade_Internship")
    private Float comGradeInternship;
    @Column(name = "specification_Internship")
    private Boolean specificationInternship;
    @Column(name = "visitSheet_Internship")
    private Boolean visitSheetInternship;
    @Column(name = "evalEntrSheet_Internship")
    private Boolean evalEntrSheetInternship;
    @Column(name = "webSurvey_Internship")
    private Boolean webSurveyInternship;
    @Column(name = "report_Internship")
    private Boolean reportInternship;
    @Column(name = "defense_Internship")
    private Boolean defenseInternship;
    @Column(name = "visitPlanified_Internship")
    private Boolean visitPlanifiedInternship;
    @Column(name = "visitDone_Internship")
    private Boolean visitDoneInternship;

    public InternshipJPA() {
    }
    

    public InternshipJPA(Integer idInternship) {
        this.idInternship = idInternship;
    }

    public Integer getIdInternship() {
        return idInternship;
    }

    public void setIdInternship(Integer idInternship) {
        this.idInternship = idInternship;
    }

    public Date getDateStartInternship() {
        return dateStartInternship;
    }

    public void setDateStartInternship(Date dateStartInternship) {
        this.dateStartInternship = dateStartInternship;
    }

    public Date getDateEndInternship() {
        return dateEndInternship;
    }

    public void setDateEndInternship(Date dateEndInternship) {
        this.dateEndInternship = dateEndInternship;
    }

    public String getMissionDescriptionInternship() {
        return missionDescriptionInternship;
    }

    public void setMissionDescriptionInternship(String missionDescriptionInternship) {
        this.missionDescriptionInternship = missionDescriptionInternship;
    }

    public String getCommentInternship() {
        return commentInternship;
    }

    public void setCommentInternship(String commentInternship) {
        this.commentInternship = commentInternship;
    }

    public Float getTechGradeInternship() {
        return techGradeInternship;
    }

    public void setTechGradeInternship(Float techGradeInternship) {
        this.techGradeInternship = techGradeInternship;
    }

    public Float getComGradeInternship() {
        return comGradeInternship;
    }

    public void setComGradeInternship(Float comGradeInternship) {
        this.comGradeInternship = comGradeInternship;
    }

    public Boolean getSpecificationInternship() {
        return specificationInternship;
    }

    public void setSpecificationInternship(Boolean specificationInternship) {
        this.specificationInternship = specificationInternship;
    }

    public Boolean getVisitSheetInternship() {
        return visitSheetInternship;
    }

    public void setVisitSheetInternship(Boolean visitSheetInternship) {
        this.visitSheetInternship = visitSheetInternship;
    }

    public Boolean getEvalEntrSheetInternship() {
        return evalEntrSheetInternship;
    }

    public void setEvalEntrSheetInternship(Boolean evalEntrSheetInternship) {
        this.evalEntrSheetInternship = evalEntrSheetInternship;
    }

    public Boolean getWebSurveyInternship() {
        return webSurveyInternship;
    }

    public void setWebSurveyInternship(Boolean webSurveyInternship) {
        this.webSurveyInternship = webSurveyInternship;
    }

    public Boolean getReportInternship() {
        return reportInternship;
    }

    public void setReportInternship(Boolean reportInternship) {
        this.reportInternship = reportInternship;
    }

    public Boolean getDefenseInternship() {
        return defenseInternship;
    }

    public void setDefenseInternship(Boolean defenseInternship) {
        this.defenseInternship = defenseInternship;
    }

    public Boolean getVisitPlanifiedInternship() {
        return visitPlanifiedInternship;
    }

    public void setVisitPlanifiedInternship(Boolean visitPlanifiedInternship) {
        this.visitPlanifiedInternship = visitPlanifiedInternship;
    }

    public Boolean getVisitDoneInternship() {
        return visitDoneInternship;
    }

    public void setVisitDoneInternship(Boolean visitDoneInternship) {
        this.visitDoneInternship = visitDoneInternship;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInternship != null ? idInternship.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InternshipJPA)) {
            return false;
        }
        InternshipJPA other = (InternshipJPA) object;
        if ((this.idInternship == null && other.idInternship != null) || (this.idInternship != null && !this.idInternship.equals(other.idInternship))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.Internship[ idInternship=" + idInternship + " ]";
    }



    @XmlTransient
    public Collection<Assignedinternship> getAssignedinternshipCollection() {
        return assignedinternshipCollection;
    }

    public void setAssignedinternshipCollection(Collection<Assignedinternship> assignedinternshipCollection) {
        this.assignedinternshipCollection = assignedinternshipCollection;
    }
    
    
    
    
}
