/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Maxence Chambrin
 */
@Entity
@Table(name = "intern")
@NamedQueries({
    @NamedQuery(name = "Intern.findAll", query = "SELECT i FROM InternJPA i"),
    @NamedQuery(name = "Intern.findByIdIntern", query = "SELECT i FROM InternJPA i WHERE i.idIntern = :idIntern"),
    @NamedQuery(name = "Intern.findByFirstnameIntern", query = "SELECT i FROM InternJPA i WHERE i.firstnameIntern = :firstnameIntern"),
    @NamedQuery(name = "Intern.findByLastnameIntern", query = "SELECT i FROM InternJPA i WHERE i.lastnameIntern = :lastnameIntern"),
    @NamedQuery(name = "Intern.findByLinkedinIntern", query = "SELECT i FROM InternJPA i WHERE i.linkedinIntern = :linkedinIntern")})
public class InternJPA implements Serializable {

    @Size(max = 255)
    @Column(name = "firstname_Intern")
    private String firstnameIntern;
    @Size(max = 255)
    @Column(name = "lastname_Intern")
    private String lastnameIntern;
    @Size(max = 255)
    @Column(name = "linkedin_Intern")
    private String linkedinIntern;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "internJPA")
    private Collection<Assignedinternship> assignedinternshipCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Intern")
    private Integer idIntern;
    @JoinTable(name = "assignedintern", joinColumns = {
        @JoinColumn(name = "id_Intern", referencedColumnName = "id_Intern")}, inverseJoinColumns = {
        @JoinColumn(name = "id_Tutor", referencedColumnName = "id_Tutor")})
    @ManyToMany
    private Collection<TutorJPA> tutorCollection;
    @JoinColumn(name = "group_Intern", referencedColumnName = "id_Group")
    @ManyToOne
    private GroupefreiJPA groupIntern;
    
    

    public InternJPA() {
    }

    public InternJPA(Integer idIntern) {
        this.idIntern = idIntern;
    }

    public Integer getIdIntern() {
        return idIntern;
    }

    public void setIdIntern(Integer idIntern) {
        this.idIntern = idIntern;
    }

    public String getFirstnameIntern() {
        return firstnameIntern;
    }

    public void setFirstnameIntern(String firstnameIntern) {
        this.firstnameIntern = firstnameIntern;
    }

    public String getLastnameIntern() {
        return lastnameIntern;
    }

    public void setLastnameIntern(String lastnameIntern) {
        this.lastnameIntern = lastnameIntern;
    }

    public String getLinkedinIntern() {
        return linkedinIntern;
    }

    public void setLinkedinIntern(String linkedinIntern) {
        this.linkedinIntern = linkedinIntern;
    }

    public Collection<TutorJPA> getTutorCollection() {
        return tutorCollection;
    }

    public void setTutorCollection(Collection<TutorJPA> tutorCollection) {
        this.tutorCollection = tutorCollection;
    }

    public GroupefreiJPA getGroupIntern() {
        return groupIntern;
    }

    public void setGroupIntern(GroupefreiJPA groupIntern) {
        this.groupIntern = groupIntern;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIntern != null ? idIntern.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InternJPA)) {
            return false;
        }
        InternJPA other = (InternJPA) object;
        if ((this.idIntern == null && other.idIntern != null) || (this.idIntern != null && !this.idIntern.equals(other.idIntern))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JPA.Intern[ idIntern=" + idIntern + " ]";
    }
    
    public void addTutor(TutorJPA tutor){
        if(tutorCollection == null)
            tutorCollection = new ArrayList<TutorJPA>();
        tutorCollection.add(tutor);
    }


    @XmlTransient
    public Collection<Assignedinternship> getAssignedinternshipCollection() {
        return assignedinternshipCollection;
    }

    public void setAssignedinternshipCollection(Collection<Assignedinternship> assignedinternshipCollection) {
        this.assignedinternshipCollection = assignedinternshipCollection;
    }
    
}
