package JPA.SessionBeans;

import JPA.TutorJPA;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Maxence Chambrin
 */
@Stateless
public class TutorFacade extends AbstractFacade<TutorJPA> {

    @PersistenceContext(unitName = "com.mycompany_AP01_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TutorFacade() {
        super(TutorJPA.class);
    }
    
    public boolean exist(String login){
        Query q = em.createNamedQuery("Tutor.findByLoginTutor");
        q.setParameter("loginTutor", login);
        boolean tutorExists = true;
        
        //If getSingleResult throws an exception the login does not exist
        try {
            q.getSingleResult();
        } catch (NoResultException e) {
            tutorExists = false;
        }
        
        return tutorExists; 
    }
            
    public TutorJPA find(int id) {
        Query q = em.createNamedQuery("Tutor.findByIdTutor");
        q.setParameter("idTutor", id);
        
        return (TutorJPA) q.getSingleResult();
    }

    public List findAll() {
        Query q = em.createNamedQuery("Tutor.findAll");
        return q.getResultList();
    }

    public TutorJPA findWithCredentials(String login, String password) {
        Query q = em.createNamedQuery("Tutor.findWithCredentials");
        q.setParameter("loginTutor", login);
        q.setParameter("passwordTutor", password);
        TutorJPA tutor = null;
        try{
            tutor = (TutorJPA) q.getSingleResult();
        }
        catch(NoResultException e){
            
        }  
        return (TutorJPA) q.getSingleResult();
    }


}
