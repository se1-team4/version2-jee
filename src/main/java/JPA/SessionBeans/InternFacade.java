/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA.SessionBeans;

import JPA.InternJPA;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Maxence Chambrin
 */
@Stateless
public class InternFacade extends AbstractFacade<InternJPA> {

    @PersistenceContext(unitName = "com.mycompany_AP01_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    private Query q;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InternFacade() {
        super(InternJPA.class);
    }
    
    
    public InternJPA find(int id){
        q = em.createNamedQuery("Intern.findByIdIntern");
        q.setParameter("idIntern", id);
        return (InternJPA) q.getSingleResult();
    }
    
    public List findAll(){
        q = em.createNamedQuery("Intern.findAll");
        return q.getResultList();
    }
    
    public void save(InternJPA intern){
        em.persist(intern);
    }
    
}
