/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA.SessionBeans;

import JPA.CompanyJPA;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Maxence Chambrin
 */
@Stateless
public class CompanyFacade extends AbstractFacade<CompanyJPA> {

    @PersistenceContext(unitName = "com.mycompany_AP01_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    private Query q;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CompanyFacade() {
        super(CompanyJPA.class);
    }
    
    
    
    public CompanyJPA find(int id){
        q = em.createNamedQuery("Company.findByIdCompany");
        q.setParameter("idCompany", id);
        return (CompanyJPA) q.getSingleResult();
    }
    
    public List findAll(){
        q = em.createNamedQuery("Company.findAll");
        return q.getResultList();
    }
    
    
}
