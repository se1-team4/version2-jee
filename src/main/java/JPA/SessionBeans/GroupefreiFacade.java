/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA.SessionBeans;

import JPA.GroupefreiJPA;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Maxence Chambrin
 */
@Stateless
public class GroupefreiFacade extends AbstractFacade<GroupefreiJPA> {

    @PersistenceContext(unitName = "com.mycompany_AP01_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    private Query q;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GroupefreiFacade() {
        super(GroupefreiJPA.class);
    }
    
    public GroupefreiJPA find(int id){
        q = em.createNamedQuery("Groupefrei.findByIdGroup");
        q.setParameter("idGroup", id);
        return (GroupefreiJPA) q.getSingleResult();
    }
    
    public List findAll(){
        q = em.createNamedQuery("Groupefrei.findAll");
        return q.getResultList();
    }
    
    public GroupefreiJPA findByName(String name){
        q = em.createNamedQuery("Groupefrei.findByNameGroup");
        q.setParameter("nameGroup", name);
        return (GroupefreiJPA) q.getSingleResult();
    }
    
}
