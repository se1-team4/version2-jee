/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA.SessionBeans;

import JPA.InternshipJPA;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Maxence Chambrin
 */
@Stateless
public class InternshipFacade extends AbstractFacade<InternshipJPA> {

    @PersistenceContext(unitName = "com.mycompany_AP01_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private Query q;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InternshipFacade() {
        super(InternshipJPA.class);
    }

    public InternshipJPA find(int id) {
        q = em.createNamedQuery("Internship.findByIdInternship");
        q.setParameter("idInternship", id);
        return (InternshipJPA) q.getSingleResult();
    }

    public List findAll() {
        q = em.createNamedQuery("Internship.findAll");
        return q.getResultList();
    }

}
