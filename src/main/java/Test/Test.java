/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;


import Models.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author trist
 */
public class Test {
    public static void main(String[] args) {
        //normalTest();

    }
    
    public static void normalTest(){
        /*OBJECT*/
        Intern intern = new Intern(0, "LINKEDIN LINK", "Gomez", "Alexandre");
        Tutor tutor = new Tutor(0, "loginLOL", "*******", "AUGUSTIN", "Jacque");
        Group group = new Group(0, "m1se");
        Internship internship = new Internship(0, "Création de site");
        Company company = new Company(0, "EDF", "3 rue petit marron", "gerard keaofdaj");
        
         /*DISPLAY BEFORE*/
        System.out.println(intern);
        System.out.println(tutor);
        System.out.println(group);
        System.out.println(internship);
        System.out.println(company);
        
        /*TEST*/
        tutor.getInterns().add(intern);
        internship.setCompany(company);
        intern.setGroup(group);
        intern.getInternships().add(internship);
        
        /*DISPLAY AFTER*/
        System.out.println(intern);
        System.out.println(tutor);
        System.out.println(group);
        System.out.println(internship);
        System.out.println(company);
        
    }
    
    
}
